# set PATH so it includes HLF bin if it exists
if [ -d "/workspaces/HLF/fabric-samples/bin" ] ; then
PATH="/workspaces/HLF/fabric-samples/bin:$PATH"
fi

chmod -R 0755 ./crypto-config
# Delete existing artifacts
rm -rf ./crypto-config
rm intellectualproperty-genesis.block intellectualproperty-channel.tx
rm -rf ../../channel-artifacts/*

#Generate Crypto artifacts for organizations
cryptogen generate --config=./crypto-config.yaml --output=./crypto-config/

# Set the path to the configtx.yaml file
export FABRIC_CFG_PATH=/workspaces/assignment1/artifacts/channe

# Generate the genesis block for the University Consortium Orderer
configtxgen -profile IntellectualPropertyOrdererGenesis -channelID ordererchannel -outputBlock intellectualproperty-genesis.block

# Create the channel NatuniChannel
configtxgen -outputCreateChannelTx ./intellectualproperty-channel.tx -profile IntellectualPropertyChannel -channelID intellectualpropertychannel

configtxgen -outputAnchorPeersUpdate contentcreatorsOrg.tx -profile IntellectualPropertyChannel -channelID intellectualpropertychannel -asOrg ContentCreatorsMSP
configtxgen -outputAnchorPeersUpdate publishersOrg.tx -profile IntellectualPropertyChannel -channelID intellectualpropertychannel -asOrg PublishersMSP
configtxgen -outputAnchorPeersUpdate regulatoryauthoritiesOrg.tx -profile IntellectualPropertyChannel -channelID intellectualpropertychannel -asOrg RegulatoryAuthoritiesMSP
